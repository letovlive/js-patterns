(function () {
  const BloodyMary = function () {
    return {
      addTomatoJuice: function () {
        console.log('juice added.');

        return this;
      },
      addWorcestershire: function () {
        console.log('worcestershire added.');

        return this;
      },
      addVodka: function () {
        console.log('vodka added.');

        return this;
      },
      serve: function () {
        console.log('Here your Marry.');
      }
    }
  };

  const cocktail = (new BloodyMary())
    .addTomatoJuice()
    .addWorcestershire()
    .addVodka()
    .serve();
}());