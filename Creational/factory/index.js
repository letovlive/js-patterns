(function () {
  const Button = function (options = {}) {
    this.button = document.createElement('button');
    this.button.style.width = (options.width || 250) + 'px';
    this.button.style.height = (options.height || 30) + 'px';
    this.button.style.background = options.background || '#ccc';
    this.button.innerText = options.text || 'Click';

    return this.button;
  };

  const Text = function (options = {}) {
    this.input = document.createElement('input');
    this.input.type = 'text';
    this.input.style.width = (options.width || 250) + 'px';
    this.input.style.height = (options.height || 30) + 'px';
    this.input.style.background = options.background || '#aaa';
    this.input.style.border = options.border || 'none';
    this.input.placeholder = options.placeholder || 'Enter text';

    return this.input;
  };

  const ControlsFactory = function () {
    return {
      build: function (controlType, options) {
        let control = {};
        switch (controlType) {
          default:
          case 'text':
            control = new Text();
            break;
          case 'button':
            control = new Button(options);
            break;
        }

        return control;
      }
    };
  };

  const factory = new ControlsFactory();

  document.body.appendChild(factory.build('text'));
  document.body.appendChild(factory.build('button', { text: 'Submit' }));
}());