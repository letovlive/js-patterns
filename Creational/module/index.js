(function () {
  const module = (function () {
    const setSize = function () {
      document.body.style.width = '80vw';
      document.body.style.height = '80vh';
    };
    const setMargin = function () {
      document.body.style.margin = '40px';
    };
    setSize();
    setMargin();
    const setBackground = function (color) {
      document.body.style.background = color || '#aaa';
    };
    return {
      setBackground: setBackground
    }
  }());

  module.setBackground('#faffff');
}());