(function () {
  const Button = {
    btn: (function () {
      return document.createElement('button');
    }()),

    setClass: function(className) {
      this.btn.className = className;

      return this;
    }
  };


  const awesomeButton = Object.create(Button);

  awesomeButton.insertTo = function (element) {
    element.append(this.btn) ;
  };

  awesomeButton.action = function (text) {
    this.btn.innerText = text;

    return this;
  };

  awesomeButton.action('click me').setClass('btn').insertTo(document.body)
}());