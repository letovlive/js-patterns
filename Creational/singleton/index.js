(function () {
  const Singleton = (function () {
    let instance;
    const state = {
      status: 'ready'
    };
    const init =function () {

      return state;
    };

    return {
      getInstance: function () {
        if (!instance) {
          instance = init();
        }

        return instance;
      }
    }
  }());

  const instanceOne = Singleton.getInstance();
  const instanceTwo = Singleton.getInstance();

  console.log(instanceOne === instanceTwo);
}());