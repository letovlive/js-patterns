(function () {
  const FoodItem = function (name) {
    this.name = name;

    this.getName = function () {
      return this.name;
    };

    return this;
  };

  const Fridge = function () {
    this.food = [];

    this.put = function (foodItem) {
      this.food.push(foodItem);
    };

    this.pick = function (name) {
      return this.food.splice(this.food.findIndex(function (item) {
        return name === item.getName();
      }), 1);
    };

    return this;
  };

  const fridge = new Fridge();
  fridge.put(new FoodItem('apple'));
  fridge.put(new FoodItem('sausages'));
  fridge.put(new FoodItem('cheese'));

  fridge.pick('apple');

  fridge.food.forEach(function (item) {
    console.log(item.getName());
  })
}());