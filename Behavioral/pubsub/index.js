(function () {
  var pubsub = (function () {
    var topics = {};

    return {
      subscribe: function (event, handler) {
        if (!topics[event]) {
          topics[event] = { queue: [] };
        }

        var index = topics[event].queue.push(handler) - 1;

        return {
          unsubscribe: function () {
            topics[event].queue.removeAt(index);
          }
        };
      },

      publish: function (event, payload) {
        if (
          topics[event] &&
          topics[event].queue.length
        ) {
          topics[event].queue.forEach(function (handler) {
            handler(payload || {});
          });
        }
      }
    };
  }());

  var subscriber = pubsub.subscribe('updated', function (data) {
    var statuses = document.getElementById('statuses');
    var status = document.createElement('p');
    status.innerText = data.message;
    statuses.appendChild(status);
  });

  setInterval(function () {
    pubsub.publish('updated', {
      message: 'data updated'
    });
  }, 2000);

  setTimeout(function () {
    subscriber.unsubscribe();
  }, 10000);
}());