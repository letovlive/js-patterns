(function () {
  const McDonalds = function () {
    this.getFood = function () {
      return 'Big Mac';
    };
    return this;
  };

  const SteakHouse = function () {
    this.getFood = function () {
      return 'Rib eye';
    };
    return this;
  };

  const haveLunch = function (moneyAmount) {
    let foodPlace;
    if (moneyAmount < 10) {
      foodPlace = new McDonalds();
    } else {
      foodPlace = new SteakHouse();
    }

    return foodPlace;
  };

  const lunchToday = haveLunch(5);
  const lunchTomorrow = haveLunch(15);

  console.log('Today we are eating ' + lunchToday.getFood());
  console.log('Tomorrow we will eating ' + lunchTomorrow.getFood());
}());