(function () {
  const Wife = function () {
   return {
     ask: function (command) {
       command.execute();
     }
   }
  };

  const Husband = function () {
    return {
      getDrunk: function () {
        console.log('I am drunk');
      },
    }
  };

  const GetBeerCommand = function (person) {
    return {
      execute: function () {
        person.getDrunk();
      }
    }
  };

  const husband = new Husband();
  const beer = new GetBeerCommand(husband);
  const wife = new Wife();

  wife.ask(beer);
}());