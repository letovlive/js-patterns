(function () {
  const Memento = function (value) {
    let content = value;

    const getContent = function () {
      return content;
    };

    return {
      getContent: getContent
    };
  };

  const Editor = function () {
    this._content = '';
    this.type = function (words) {
      this._content = this._content + ' ' + words;
    };

    this.getContent = function () {
      return this._content;
    };

    this.save = function () {
      return new Memento(this._content);
    };

    this.restore = function (memento) {
      this._content = memento.getContent();
    };

    return this;
  };

  const editor = new Editor();

  editor.type('First random phrase.');
  editor.type('Second random phrase.');

  const stored = editor.save();

  editor.type('Third random phrase.');
  console.log(editor.getContent());

  editor.restore(stored);

  console.log(editor.getContent());


}());