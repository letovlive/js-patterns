(function () {
  const RegistrationDesk = function () {
    return {
      checkIn: function (name) {
        console.log('Register ' + name + '.');
      }
    };
  };

  const Guest = function (name, registrationDesk) {
    this.name = name;
    this.register = function () {
      registrationDesk.checkIn(this.name);
    };

    return this;
  };

  const registrationDesk =  new RegistrationDesk();
  const firstGuest = new Guest('John Doe', registrationDesk);
  const secondGuest = new Guest('Jack Doe', registrationDesk);

  firstGuest.register();
  secondGuest.register();
}());