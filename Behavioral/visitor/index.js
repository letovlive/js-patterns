(function () {
  const Conditioner = function () {

    this.turnedOn = false;

    this.cooling = function (temperature) {
      console.log('Cooling to ' + temperature);
      this.turnedOn = true;
    };

    this.accept = function (operation) {
      operation.visitConditioner(this);
    };
    return this;
  };

  const Heater = function () {
    this.turnedOn = false;

    this.heating = function (temperature) {
      console.log('Heating to ' + temperature);
      this.turnedOn = true;
    };

    this.accept = function (operation) {
      operation.visitHeater(this);
    };
    return this;
  };

  const turnOn = {
    visitConditioner: function (conditioner) {
      conditioner.cooling(22);
    },
    visitHeater: function (conditioner) {
      conditioner.heating(22);
    }
  };

  const turnOff = {
    visitConditioner: function (conditioner) {
      conditioner.turnedOn = false;
      console.log('Turned off conditioner');
    },
    visitHeater: function (conditioner) {
      conditioner.turnedOn = false;

      console.log('Turned off heater');
    }
  };

  const conditioner = new Conditioner();
  const heater = new Heater();

  conditioner.accept(turnOn);
  heater.accept(turnOn);

  conditioner.accept(turnOff);
  heater.accept(turnOff);
}());