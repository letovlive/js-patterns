(function () {
  const Delivery = {
    tryNext (account) {
      this.receiver = account;

      return this.receiver;
    },

    deliver (totalCount) {
      if (this.canDeliver(totalCount)) {
        console.log(`We can deliver your ${totalCount} pizza${totalCount > 1 ? 's' : ''} using ${this.name}`)
      } else if (this.receiver) {
        console.log(`Cannot deliver using ${this.name}. Try another delivery type...`);
        this.receiver.deliver(totalCount)
      } else {
        console.log('')
      }
    },

    canDeliver(count) {
      return this.pizzaCount >= count
    }
  };

  const PublicTransportDelivery = function (pizzaCount) {
    Object.setPrototypeOf(this, Delivery);
    this.name = 'Public Transport delivery';
    this.pizzaCount = pizzaCount;
  };

  const BicycleDelivery = function (pizzaCount) {
    Object.setPrototypeOf(this, Delivery);
    this.name = 'Bicycle delivery';
    this.pizzaCount = pizzaCount;
  };

  const CarDelivery = function (pizzaCount) {
    Object.setPrototypeOf(this, Delivery);
    this.name = 'Car delivery';
    this.pizzaCount = pizzaCount;
  };

  const publicTransportDelivery = new PublicTransportDelivery(5);
  const bicycleDelivery = new BicycleDelivery(10);
  const carDelivery = new CarDelivery(30);

  publicTransportDelivery.tryNext(bicycleDelivery).tryNext(carDelivery);

  publicTransportDelivery.deliver(25);

}());