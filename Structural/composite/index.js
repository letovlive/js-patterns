(function () {
  const Knife = function (brand) {
    this.brand = brand;

    return {
      chop: function () {
        console.log('Chopping');
      },
      slice: function () {
        console.log('Slicing');
      },
    };
  };

  const FoodProcessor = function (brand) {
    this.brand = brand;

    return {
      chop: function () {
        console.log('Chopping');
      },
      mesh: function () {
        console.log('Meshing');
      },
    };
  };

  const Chief = function () {
    const appliances = [];

    return {
      addAccessory: function (object) {
        appliances.push(object);
      },
      chopFood: function () {
        appliances.forEach(function (appliance) {
          appliance.chop();
        })
      }
    };
  };

  const chiefGarry = new Chief();
  chiefGarry.addAccessory(new Knife('Tefal'));
  chiefGarry.addAccessory(new FoodProcessor('Phillips'));

  chiefGarry.chopFood();
}());