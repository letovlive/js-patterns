(function () {
  const Soup = function () {
    return {
      getCalories () {
        return 250;
      }
    }
  };

  const SoupWithMeat = function (soup) {

    return {
      getCalories () {
        return soup.getCalories() + 300;
      }
    }
  };

  const SoupWithMeatAndCream = function (soup) {
    return {
      getCalories () {
        return soup.getCalories() + 180;
      }
    }
  };

  let soup;

  soup = new Soup();
  console.log(soup.getCalories());

  soup = new SoupWithMeat(soup);
  console.log(soup.getCalories());

  soup = new SoupWithMeatAndCream(soup);
  console.log(soup.getCalories());
}());