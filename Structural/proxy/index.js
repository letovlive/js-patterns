(function () {
  const Bartender = function () {
    return {
      orderCocktail: function () {
        return 'here your cocktail';
      }
    };
  };

  const Waiter = function (bartender) {
    return {
      orderCocktail: function (ice) {
        return ice ? (bartender.orderCocktail() + ' with ice') : bartender.orderCocktail();
      }
    };
  };
  
  const bartender = new Bartender();
  const waiter = new Waiter(bartender);

  console.log(waiter.orderCocktail('add ice'));
}());