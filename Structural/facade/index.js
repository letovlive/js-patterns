(function () {
  const Pizza = function () {
    return {
      kneadDough: function () {
        console.log('knead dough');
        return this;
      },
      makeSouce: function () {
        console.log('make sauce');
        return this;
      },
      grateChees: function () {
        console.log('grate cheese');
        return this;
      },
      collect: function () {
        console.log('collect pizza');
        return this;
      },
      bake: function () {
        console.log('bake');
        console.log('Here your pizza!');
        return this;
      }
    }
  };

  const PizzaDecorator = function () {
    return {
      getHotPizza: function () {
        return new Pizza()
          .kneadDough()
          .makeSouce()
          .grateChees()
          .collect()
          .bake();
      }
    }
  };

  const freshPizza = new PizzaDecorator().getHotPizza();
}());