(function () {
  const Programmer = function (food) {
    food.eat();
  };

  const Pizza = (function () {
    return {
      eat: function () {
        console.log('I am eating pizza');
      }
    }
  }());

  const Sand = (function () {
    return {
      eat: function () {
        console.log('I am eating sand.');
      }
    }
  }());

  const goodProgrammer = new Programmer(Pizza);
  const badProgrammer = new Programmer(Sand);
}());