(function () {
  const githubProfile = {
    get: function (name) {
      return fetch('https://api.github.com/users/' + name, {
        headers: {
          Accept: 'application/vnd.github.v3+json'
        }
      })
        .then(function (response) { return response.json(); })
        .then(function (details) { return details;});
    }
  };

  const gitlabProfile = {
    get: function (name) {
      return fetch('https://gitlab.com/api/v4/users?username=' + name)
        .then((response) => response.json())
        .then((details) => {
          return details[0];
        });
    }
  };

  const getDetails = function (name) {
    showResponse = function (json) {
      const container = document.createElement('textarea');
      container.value = JSON.stringify(json, undefined, 4);
      document.querySelector('div').appendChild(container);
    };
    githubProfile.get(name).then(response => showResponse(response));
    gitlabProfile.get(name).then(response => showResponse(response));

  };
  const button = document.querySelector('button');
  button.addEventListener('click', function () {
    getDetails(document.querySelector('input[name=profile]').value);
  });
}());